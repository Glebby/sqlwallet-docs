.. SQLwallet documentation master file, created by
   sphinx-quickstart on Fri Dec  1 17:29:17 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

*This documentation is still under development. The new articles are being added regularly.*

Contents
========

.. toctree::
   :maxdepth: 2

   installation/index  
   design/index
   reports/index
   embedding
 

 

