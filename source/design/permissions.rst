.. _authorization:

Permissions
=============

.. image:: images/permissions.png

To grant specific permissions for a report to users or teams:

#. Click "Permissions" in the header of the report.
#. Choose a type of permission in the upper left dropdown. The possible permissions are:
    * **Run** - allows to execute the report.
    * **Design** - allows to modify the report.
    * **View** - allows to view the report definition (design view) but not to modify it. 
    * **DataEdit** - for editable reports, allows to edit existing records.
    * **DataInsert** - for editable reports, allows to create new records.
    * **DataRemove** - for editable reports, allows to delete records (if data deletion is enabled in the report settings).
    * **DataImport** - for editable reports, allows to import records from Excel or CSV (if data import is enabled in the report settings).
    * **Grant** - allows user to manage permissions for this report. The users can only assign permissions that they have by themselves. For example, if a user has Run permission, they can grant the Run permission to other users, but cannot grant Design permission.
#. Select the users/teams you want to assign the permission. You can filter by name to quickly locate the user or team.
#. Repeat steps 2 - 3 for different permissions.
#. Click Save.

.. note::
 You also can assign global permissions to users in the :ref:`configuration` section, such as Run All, View All etc. In this case you do not need to assign permissions to the user for each report separately.

.. note:: 
 Use "i" icon next to the team name to quickly check the users, that are members of this team.


