.. _data_editing:

Data editing
============
 
Report settings
---------------

SQLwallet allows you to edit underlying data in the table reports, effectively turning a report into data editing app, that can be useful in scenarios when you need to quickly provide your customers with simple data editing pages or admin apps. The editing access can be limited to specific users or teams only in the :ref:`authorization` section.

.. image:: images/data_editing.png

* **Allow data editing** - enables data editing for the report. After enabling it, you need to specify editable columns individually in `Column settings`_ section.
* **Inline editing** - if enabled, the users will be able to edit the values directly in table cells. Otherwise, a pencil icon will be displayed in each table row, and a popup window will be opened to edit the record.
* **Allow delete records** - if enabled, a Delete icon will be displayed in each table row. Deleting records can be limited to specific users or teams only in the :ref:`authorization` section.
* **Allow data import** - if enabled, the users will be able to `import data </import>`_ from Excel and CSV files to add new records or update existing records. Data importing can be limited to specific users or teams only in the :ref:`authorization` section. Also you will need to specify which columns are allowed to be imported in Output Columns section.
* **PK field name** - specifies the primary/unique key column in the underlying table. This setting can also be set for each individual column. The column **must** be present in the SQL query.
* **Main table name** - specifies the name of the table in the database, that contains the data to be edited. This setting can also be set for each individual column.
 
Column settings
---------------
After you enable **Allow data editing** checkbox, you will need to define all editable columns under :ref:`output_columns` section. 

.. image:: images/editable_column.png

* **Edit as** - specifies the input type (String, Dropdown, DatePicker, Checkbox, Textarea) for the column.
* **Values** - allows to specify list of possible values for dropdowns (see "Multi-value parameters" in the :ref:`parameters` section).
* **Edit column name** - specifies the column name, which contains the data in the underlying table (if it is different from the column name returned by the report SQL query)
* **Edit table name** - specifies the name of the table in the database, that contains the data to be edited (if different from the **Main table name** field in the `Report settings`_).
* **PK field name** - specifies the primary/unique key column in the underlying table (if different from the **PK field  name** field in the `Report settings`_). The column **must** be present in the SQL query.
* **Editable in a new record** - if enabled, then the column will be available for editing when the user creates a new record. 

.. note:: 
 You can hide primary key columns from the output using "Hidden" checkbox into Output Columns section.
 


