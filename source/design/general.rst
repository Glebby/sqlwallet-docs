.. _general:

General
=======

This section describes usage of the general report settings.

.. image:: images/general.png

* **Report title** - specifies how the report will appear in the navigation tree.
* **Description** - short report description that will appear above report parameters. Use this to provide instructions or clarify the report purpose for the users.
* **Export file name** - here you can specify the file name for export into Excel or CSV. You can use macro parameters (see below) in this field, to specify date of the export or other dynamic values. If not specified, the report tile will be used as the export file name.
* **Run immediately** - if enabled, the report will be executed on screen immediately, otherwise the user will need to click Run button to execute the report. Use this option if the report does not have mandatory parameters and if the report execution time is quick.
* **Report type** -  specifies type of the report:
    - **SQL report** - the report is based on SQL query. This is the most common report type.
    - **External page** - the report is hosted on an external website and will be shown in IFRAME. You can send parameters from SQLwallet into the report webpage. Use this option if you want quickly add your legacy reports into SQLwallet portal.
**Allow scheduling** - if enabled, the authorized users will be able to :ref:`schedule` the report.
* **Refresh automatically** - if enabled, the report on screen will be auto-refreshed on the specified intervals of time. 


Examples of macro parameters that can be used in the Export file name
----------------------------------------------------------------------

================================  =============
Macro                             Meaning
================================  =============
${now}                            Current date/time (at the moment of the export, e.g. 23/12/2018 17:45)  
${yesterday}                      Yesterday's date (e.g. 22/12/2018)
${today:yyyy-MM-dd}               Today's date formatted (e.g. 2018-12-23)
${param.DateFrom}                 Value of report parameter "DateFrom" (e.g. 23/12/2018)
${param.DateFrom:yyyy-MM-dd}      Value of report parameter "DateFrom", formatted (e.g. 2018-12-23)
================================  =============
 