.. _report_output:

Report output
=============

.. image:: images/report_output.png

The report output consists of one or more tabs/widgets. Each tab has its own data source SQL query, output columns and editing options. The SQL queries in all tabs has access to  the same set of report parameters.

When users run the report they can rearrange the tabs to be shown alongside each other, like widgets in a dashboard.

When the report is exported as Excel, the tabs will be exported as separate sheet within the Excel file. When the report is exported as CSV, each tab will be exported a separate CSV file, and all the files will be packed into a ZIP archive.  

* **Tab/Widget caption** - the title of the tab/widget. If the report has only single tab, the title will not be shown.

* **Default view** - specifies the default view of the report, either flat table, or pivot/chart for data visualization.

* **Allow users to change view** - if enabled, the users will be able to switch between Table view and Pivot/Chart view.

* **Allow users to modify pivot/chart** - if enabled, the pivot/chart view will become interactive, allowing the users to manipulate the data aggregates and build their own data visualization.

Under each tab there are the following sections:

* :ref:`sql_query` - contains the data source and the SQL query, used to produce the report output.
* :ref:`output_columns` - contains definition of output columns (optional)
* :ref:`data_editing` - allows to enable data editing within the report output. 

