.. _output_columns:

Output columns
=================

By default SQLwallet will show all columns returned by the query, with default formatting. So you don't have to define output columns unless you want to override appearance of a specific column. You need only to add output columns for the columns you want to override.

.. image:: images/output_columns.png

Output column settings: 

* **Column name** - this should match the exact name of the column as returned by the SQL query.
* **Display name** - use this to override the column name.
* **Width** - initial screen column width in percentage or pixels, for example 10%, or 200px. 
* **Screen format** - use to format the output values on screen. See the available format patterns `here <https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings>`_.
* **Excel format** - use to format the output values when exporting into Excel/CSV, if different from the screen format. See the available format patterns `here <https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings>`_.
* **HTML** - enable if the column values contain HTML mark-up (for example hyperlinks) and you want it to be displayed as HTML.
 .. note:: 
  Use HTML columns with caution, as HTML content can potentially be harmful. Make sure that the content you are displaying is safe.
* **Hidden** - enable this to hide the column from the output.  
* **Editable** - enable this to allow users to edit the data in the column. Only visible is Data Editing is enabled  (see :ref:`data_editing` section).
* **Importable** - enable this to allow users to import the data in the column from Excel or CSV. Only visible is Data Import is enabled  (see :ref:`data_editing` section).

All output column fields may be left empty, except "Column name".

.. note:: 
 You cannot change order of output columns in this section. If you need to change the order, you should modify the SQL query.  