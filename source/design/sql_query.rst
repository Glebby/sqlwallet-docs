.. _sql_query:

SQL query
==========

.. image:: images/sql_query.png 

* **Data source** - select the :ref:`data_source` (database connection) which will be used to run the SQL query. For your reference, the connection string for the selected data source will be shown below. The data sources are managed in the SQLwallet :ref:`configuration` section.

* **Query type** - select "Query" or "Stored procedure". See more about the difference between these two options in the `Multi-value parameters <parameters>`_  section.
 
* **SQL query** - contains the SQL query used to retrieve the data from the database specified in the Data Source. You can use the report parameters `variables and macros <parameters>`_ in the SQL query. For more details, please refer to the :ref:`parameters` section.

.. note:: 
 You can use multiple SQL statements, including declaration of variables, and T-SQL commands, such as IF, BEGIN/END, etc, if the SQL dialect supports it.