.. _import_export:

Import and export
====================

Sometimes you want to create a new report based on an existing one, or copy the report into another folder, or into another instance of SQLwallet.

.. rubric:: Export

To export a report, switch into Design mode and click "Export" button at the bottom of the page. Then select a location on your computer to save the imported file. 

.. rubric:: Import

To import a previously exported report:

#. In the navigation tree select the folder, where you want to place the new report, and click "New report button".
#. Click "Import from..." button at the bottom of the page and select the file to import.
#. Click "Confirm import" button. 
#. Make any amendments if necessary, and click "Save".

.. note::
 SQLwallet will try to match the data sources used in the exported report (in the main SQL query, or in multi-value parameters, or in editable columns), if such data sources exists. If data source with the same name does not exists, you will need to specify the new data sources before saving the exported report, otherwise you will receive an error message that the data source is missing.





