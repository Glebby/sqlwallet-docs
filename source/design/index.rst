Creating reports
=================

To create a new report, click "New Report" button when you are in the root or in any folder in the navigation tree  (if you don't see this button, that means you are not allowed to create new reports).

To modify a report, click "Design" button in the report header (if you don't see this button, that means you are not   allowed to change this report).

**Design report sections:**

.. toctree::
   :maxdepth: 1

   general
   parameters
   report_output
   sql_query
   output_columns
   data_editing
   import_export
   permissions