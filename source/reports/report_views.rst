.. _execution:

Report views
=============

The default view of the report output is specified in :ref:`report_output` section. Users can switch between views using the report toolbar icons, if the report allows it.

Table View
-----------

.. image:: images/table_view.png

Actions available in the Table view:

* **Sorting** - users can change the default order of rows by clicking |icon_sort| icon to sort by this column in ascending or descending order. It is possible to sort by multiple columns with holding Shift button. SQLwallet will remember the selected sorting order until the user resets the report layout by clicking |icon_reset| icon.
* **Resizing** - users can resize columns by dragging column borders in the table header. SQLwallet will remember the changed column widths until the user resets the report layout  by clicking |icon_reset| icon.
* **Hiding columns** - user can hide columns by pointing mouse over the column header and clicking "x" icon. SQLwallet will remember the hidden columns until the user resets the report layout  by clicking |icon_reset| icon.
* **Quick Filter** - in addition to the main report filters, users can filter by any column in the table. To open the quick filter panel, point the mouse between the table header and the first row.

 .. image:: images/filter.png

.. note:: 
 The table operations are performed in the browser. They do not cause the report to be executed again on the server, but they can take longer time if your dataset is big.  If you want to implement sorting and filtering on server side, you should do so within SQL query and allow users to apply filters and sorting using the report parameters and macros.

 By default the screen report output is limited by 2000 records. If the report produces more records, a user will get a warning message. The quick filtering and sorting is performed on the displayed records only.


.. |icon_sort| image:: images/icon_sort.png 
               :align: middle
               :height: 17px 


Pivot/Chart View
----------------

.. image:: images/chart.png

In Pivot/Chat view users can select the data aggregation and visualization (this can be enabled or disabled in :ref:`report_output` section).

The following charts and visualizations are supported:

* **Table** - the data is aggregated by selected columns and displayed as a pivot table.
* **Table Bar Chart** - the data is aggregated by selected columns and displayed as a pivot table. Each sell contains a bar chart indicating the value weight in the row.
* **Table Heatmap** - the data is aggregated by selected columns and displayed as a pivot table. Each sell is highlighted according to the value weight in the whole table.
* **Table Heatmap** - the data is aggregated by selected columns and displayed as a pivot table. Each sell is highlighted according to the value weight in the row.
* **Column Heatmap**- the data is aggregated by selected columns and displayed as a pivot table. Each sell is highlighted according to the value weight in the column.
* **TSV Export** - the pivoted data in text format that can be copied and pasted into Excel.
* **Bar Chart** 
* **Stacked Bar Chart** 
* **Horizontal Bar Chart** 
* **Horizontal Stacked Bar Chart** 
* **Line Chart**
* **Area Chart**
* **Scatter Chart**

The user can select the metric column and the aggregate function (Sum, Average, Count, Unique Count, Min, Max etc). At the moment only single metric is supported per chart.

The user can select data series on both X and Y axes  by dragging the column titles from the top area onto the rows/columns areas.

The following commands are available in the pivot/chart toolbar:

* |icon_print| - prints the chart. Please note that for large charts the user may need to adjust the scale and page orientation in the Print dialog in order to fit the whole chart on the page.
 
* |icon_image| - exports the chart as image file.

* |icon_pin| - saves the current pivot/chart layout as a default. Only available for the report authors.

* |icon_table| - switches to the table view.

.. |icon_print| image:: images/icon_print.png 
               :align: middle
               :height: 20px 

.. |icon_image| image:: images/icon_image.png 
              :align: middle
              :height: 20px  

.. |icon_pin| image:: images/icon_pin.png
              :align: middle
              :height: 20px               

.. |icon_table| image:: images/icon_table.png
              :align: middle
              :height: 20px               

.. |icon_reset| image:: images/icon_reset.png
              :align: middle
              :height: 16px           