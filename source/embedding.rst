.. _embedding:

Embedding reports
=================

SQLwallet allows you to invoke reports directly from your application by adding a link  
to a specific report or by embedding the report page in IFRAME.

The URL for direct link to the report looks like::

   http://yoursqlwalletdomain.com/run/<reportid>?access_token=<access token>


You can obtain the URL for the specific report using "Get direct link" button at the bottom of the Report Designer page.

Generating access token
-----------------------

To get access token, perform the following steps:

**1. Invoke the token API request (server-to-server request) to obtain authorization token**:
   
.. code-block:: guess

   POST /connect/token HTTP/1.1
   Host: yoursqlwalletdomain.com:88
   Content-Type: application/x-www-form-urlencoded
   Cache-Control: no-cache

   client_secret=<secret>&grant_type=client_credentials&client_id=sqlwallet&scope=sqlwallet
   

**2. Invoke the API method (server-to-server request) to get the access token to run report on behalf of a specific user**:

.. code-block:: guess

   POST /auth/accesstoken/<username> HTTP/1.1
   Host: yoursqlwalletdomain.com:88
   Content-Type: application/json
   Cache-Control: no-cache
   Authorization: Bearer <authorization token>

   {"param1":"value1","param2":"value2",...}

.. note::

   * Authentication API calls shoudl be made using the primary domain SQLwallet is installed on (i.e. defined in appsettings.json)
	   
Add report parameters in the request body as shown above. These parameters will be encrypted in the token, so users
cannot change them in runtime. 

Invoking the report
-------------------

Invoke the report using the generated access token::

   http://yoursqlwalletdomain.com/run/<reportid>?access_token=<access token>

If you want to be able to modify the parameters in the client side code, 
you can pass them directly in the report URL::

   http://yoursqlwalletdomain.com/run/<reportid>?access_token=<access token>&param1=value1&param2=value2...

.. note::

   * Parameters in the token will take precedence over parameters passed in query string.
   * You can also pass access token to the report in the Authorization Bearer request header.

Alternatively, you can invoke the report with parameters panel visible, so users can modify  report parameters::

    http://yoursqlwalletdomain.com/embed?access_token=<access token>#<reportid>

