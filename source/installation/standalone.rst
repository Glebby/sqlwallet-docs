Windows Server - Standalone Service
===================================

This article explains how to install and run SQLwallet as a standalone Windows Service.

*Prerequisites: Windows Server 2008 R2 or later (Windows 7 SP1 or later). No IIS required.*

1. Download SQLwallet installation executable from the `Downloads <https://www.sqlwallet.com/download>`_ page and run the downloaded file.
2. Follow the installation wizard instructions. When asked to enter the server hostname and port, specify the hostname and port you will be running SQLwallet instance. Make sure the port is not used by any other applications. We suggest port 88 as the default one. 
 .. note:: 
    You can modify these settings later in appsettings.json file.
3. Once the installation finishes, a browser window will open and SQLwallet will start. On the first run you will be prompted to enter the admin user password.

If the browser window does not open automatically, try to launch SQLwallet site manually by navigating to http://hostname:port in the browser, for example http://localhost:88.

In case SQLwallet website fails to start, please make sure that SQLwallet service is running. If the service fails to start, check the application and system logs under Windows Event Viewer and the logs folder under SQLwallet installation folder, that can help you with the troubleshooting.

To upgrade, run the setup file and follow the instructions. Make sure to always check the special upgrade instructions and information of any manual steps and breaking changes in the `Release Notes <https://www.sqlwallet.com/direct-download/>`_. 