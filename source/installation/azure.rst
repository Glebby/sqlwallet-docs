Azure App Service
====================

This article explains how to install and run SQLwallet as an Azure App Service.

*Prerequisite: you should have an active Azure subscription.*

#. On `Azure Portal <https://portal.azure.com>`_ create a new "Microsoft Web App" `app service <https://docs.microsoft.com/en-us/azure/app-service/>`_. We recommend to use at least S2 pricing tier (3.5 GB memory).
#. Download SQLwallet ZIP archive from the `Downloads <https://www.sqlwallet.com/download>`_ page and extract its content into a local folder on your computer. Makes sure you `unblock the archive <https://blogs.msdn.microsoft.com/delay/p/unblockingdownloadedfile/>`_ before unpacking it.
#. Deploy the content of the folder to your App Service using FTP (`here is how <https://docs.microsoft.com/en-us/azure/app-service/deploy-ftp>`_).
#. Navigate to the web app URL to check that SQLwallet website is up and running. If the service does not start, use `this troubleshooting guide <https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/azure-apps/troubleshoot>`_.

.. note::
 When running SQLwallet as an Azure App Service, leave ServerHostName and ServerPort parameters empty in appsettings.json file.

To upgrade, follow the steps 2 - 4 above and upload the ZIP archive content over the existing files. If there are any locked files preventing the copying, stop the Azure App Service service before the upgrading. Alternatively, you can use `deployment slots technique <https://docs.microsoft.com/en-us/azure/app-service/deploy-staging-slots>`_.