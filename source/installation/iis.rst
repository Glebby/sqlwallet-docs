Windows Server - IIS
====================

This article explains how to install and run SQLwallet under Internet Information Services on Windows machines. 

*Prerequisites: Windows Server 2008 R2 or later (Windows 7 SP1 or later), with IIS installed.*

#. Download and install `.NET Core 2.x Runtime & Hosting bundle <https://www.microsoft.com/net/permalink/dotnetcore-current-windows-runtime-bundle-installer>`_. If you already have this installed, skip this step.
#. Download SQLwallet ZIP archive from the `Downloads <https://www.sqlwallet.com/download>`_ page and extract its content into a local folder. 

 .. note:: Makes sure you `unblock the archive <https://blogs.msdn.microsoft.com/delay/p/unblockingdownloadedfile/>`_ before unpacking it!

#. In IIS manager, create a new website pointing to the folder with SQLwallet files and change the Application Pool to "No Managed Code". For the details, please refer to the  `article on Microsoft website <https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/iis>`_.
#. `Grant Modify permissions <https://docs.microsoft.com/en-us/iis/manage/configuring-security/application-pool-identities>`_ to the app pool identity user ("IIS AppPool\\<your_apppool_name>") for the SQLwallet root folder.
#. Navigate to the website URL to check that SQLwallet website is up and running.

.. note::
 When running SQLwallet under IIS, leave ServerHostName and ServerPort parameters empty in appsettings.json file.


To upgrade, follow the step 2 above and copy the ZIP archive content over the existing files. If there are any locked files preventing the copying, stop the IIS service before the upgrading.



