.. SQLwallet documentation master file, created by
   sphinx-quickstart on Fri Dec  1 17:29:17 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Contents
========

.. toctree::
   :maxdepth: 2

   embedding
	configuration

 
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
